#include <iostream>
#include "stdio.h"
#include "stdlib.h"
#include "time.h"
#include <math.h>
#include <sys/time.h>
#include "pthread.h"

#define SIZE 1048576

using namespace std;


float* random_generator() { 
	float* arr = new float[SIZE];   
	for(int i=0; i<SIZE; i++)
		arr[i] = float(rand())/float((RAND_MAX)) * 1000000;
	return arr; 
} 

float* copy_float_array(float *arr) { 
	float* copy = new float[SIZE];   
	for(int i=0; i<SIZE; i++)
		copy[i] = arr[i] ;
	return copy; 
} 

void print(float *arr) {
	for(int i=0; i<SIZE; i++)
		printf("%f\n", arr[i]);
}

int partition(float *random_number, int start, int end) {
	int p = random_number[end];
	int i = start-1;
	for (int j=start; j<end; j++) {
		if (random_number[j] < p) {
			i = i+1;
			float temp = random_number[i];
			random_number[i] = random_number[j];
			random_number[j] = temp;
		}
	}
	random_number[end] = random_number[i+1];
	random_number[i+1] = p;
	return (i+1);
}


void quick_sort(float *random_number, int start, int end) {
	if (start < end) {
		int p = partition(random_number, start, end);
		quick_sort(random_number, start, p-1);
		quick_sort(random_number, p+1, end);
	}
}

	
typedef struct {
	float *random_number;
	int start;
	int end;
}part_of_random_number;


void *parallel_quick_sort(void* arg) {
	part_of_random_number *r = (part_of_random_number *)arg;

	if ((r->start) < (r->end)) {
		if ((r->end - r->start) <= 17000) {
			quick_sort(r->random_number, r->start, r->end);
		}
		else {
			int p = partition(r->random_number, r->start, r->end);
			pthread_t th1;
			pthread_t th2;
			part_of_random_number r1 = {r->random_number, r->start, p-1};
			part_of_random_number r2 = {r->random_number, p+1, r->end};
			pthread_create(&th1, NULL, parallel_quick_sort, &r1);
			pthread_create(&th2, NULL, parallel_quick_sort, &r2);
			pthread_join(th1, NULL);
			pthread_join(th2, NULL);			
		}
	}
	pthread_exit(NULL);
}


long part2_parallel(float* random_number) {
	struct timeval start, end;
	gettimeofday(&start, NULL);
	part_of_random_number main_part = {random_number, 0, SIZE-1};
	pthread_t first_th;
	pthread_create(&first_th, NULL, parallel_quick_sort, &main_part);
	pthread_join(first_th, NULL);
	gettimeofday(&end, NULL);
	long micros = ((end.tv_sec - start.tv_sec) * 1000000) + (end.tv_usec - start.tv_usec);
	return micros;
}


long part2_serial(float* random_number) {
	struct timeval start, end;
	gettimeofday(&start, NULL);
	quick_sort(random_number, 0, SIZE-1);
	gettimeofday(&end, NULL);
	long micros = ((end.tv_sec - start.tv_sec) * 1000000) + (end.tv_usec - start.tv_usec);
	return micros;
}


float part2() {
	float* r = random_generator();
	float* rs = copy_float_array(r);
	float* rp = copy_float_array(r);
	long sTime = part2_serial(rs);
	long pTime = part2_parallel(rp);
    return float(sTime)/float(pTime);
}


int main() { 
	printf("MirHamed Jafarzadeh Asl: 810096008\n");
	printf("Seyede Mehrnaz Shamsabadi: 810196493\n");

	printf("**************************************\n");

	printf("Part 2:\n");
	float speed_up_part2 = 0;
	for (int i=0; i<10; i++)
		speed_up_part2 = speed_up_part2 + part2();
	printf("\nAverage speed Up of Part 2: %f\n", speed_up_part2/10);
	
	return 0; 
}