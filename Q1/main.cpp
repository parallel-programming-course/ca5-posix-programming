#include <iostream>
#include "stdio.h"
#include "stdlib.h"
#include "time.h"
#include <math.h>
#include <sys/time.h>
#include "pthread.h"

#define SIZE 1048576
#define NUMBER_OF_THREADS 4

typedef struct{
	float max;
	long index; 
} max__val_ind;

typedef struct{
	max__val_ind *ptr;
	float* array;
	long start_index;
	long end_index;
} in_param_t;

pthread_mutex_t lock;

using namespace std;

float* random_generator() { 
	float* arr = new float[SIZE];
	for(long i=0; i<SIZE; i++)
		arr[i] = float(rand())/float((RAND_MAX)) * 1000000;
	return arr; 
}

long serial_run(float * random_numbers, bool print) {
	struct timeval start, end;
		
	gettimeofday(&start, NULL);

	float max = random_numbers[0];
	long index = 0;

	for (long j=1; j<SIZE; j++) {
		if (random_numbers[j] > max) {
			max = random_numbers[j];
			index = j;
		}
	}

	gettimeofday(&end, NULL);
	long micros = ((end.tv_sec - start.tv_sec) * 1000000) + (end.tv_usec - start.tv_usec);

	if (print == true) {
		printf("\nSerial:\n");
		printf("Maximum: %f , Index: %ld\n", max, index);
		printf("Time: %ld microsecond\n", micros);
	}

	return micros;
}

void *find_max(void *arg) {

	in_param_t *inp = (in_param_t *) arg;

	float localMax = inp->array[inp->start_index];
	long localIndex = inp->start_index;
	
	for (long j=inp->start_index; j<inp->end_index; j++) {
		if (inp->array[j] > localMax) {
			localMax = inp->array[j];
			localIndex = j;
		}
	}

	pthread_mutex_lock(&lock);
	if (localMax > inp->ptr->max) {
		inp->ptr->max = localMax;
		inp->ptr->index = localIndex;
	}
	pthread_mutex_unlock(&lock);

	pthread_exit(0);
}

long parallel_run(float* random_numbers, bool print) {
	struct timeval start, end;
	
	gettimeofday(&start, NULL);

	max__val_ind final_max = {random_numbers[0], 0};
	max__val_ind *ptr = &final_max;

	pthread_t thrs[NUMBER_OF_THREADS];
	in_param_t input_params[NUMBER_OF_THREADS];

	pthread_mutex_init(&lock, NULL);

	for (long i = 0; i < NUMBER_OF_THREADS; i++)
		input_params[i] = {ptr, random_numbers, i*SIZE/NUMBER_OF_THREADS, (i+1)*SIZE/NUMBER_OF_THREADS};

	for (long i = 0; i < NUMBER_OF_THREADS; i++)
		pthread_create(&thrs[i], NULL, find_max,(void *) &input_params[i]);

	for (long i = 0; i < NUMBER_OF_THREADS; i++)
		pthread_join(thrs[i], NULL);
	
	pthread_mutex_destroy(&lock);

	gettimeofday(&end, NULL);
	long micros = ((end.tv_sec - start.tv_sec) * 1000000) + (end.tv_usec - start.tv_usec);

	if (print == true) {
		printf("\nParallel:\n");
		printf("Maximum: %f , Index: %ld\n", ptr->max, ptr->index);
		printf("Time: %ld microsecond\n", micros);
	}

	return micros;
}

float part1() {
	float* r = random_generator();
	long sTime = serial_run(r, false);
	long pTime = parallel_run(r, false);
	return float(sTime)/float(pTime);
}


int main() { 
	printf("MirHamed Jafarzadeh Asl: 810096008\n");
	printf("Seyede Mehrnaz Shamsabadi: 810196493\n");

	printf("**************************************\n");

	printf("Part 1:\n");
	float speed_up_part1 = 0;
	for (int i=0; i<200; i++)
		speed_up_part1 = speed_up_part1 + part1();
	printf("\nAverage speed Up of Part 1 for %d runs: %f\n", 200, speed_up_part1/200);
	
	return 0; 
}
